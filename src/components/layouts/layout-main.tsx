import React from "react";
import { Layout } from "antd";
import LayoutMenu from "./layout-menu";
import LayoutHeader from "./layout-header";
import LayoutContent from "./layout-content";
type Props = {
  children: React.ReactNode;
  setTheme : any
};

export default function LayoutMain({children,setTheme}: Props) {
  return (
    <Layout style={{ height: "100vh" }}>
      <LayoutMenu/>
      <Layout>
        <LayoutHeader setTheme={setTheme}/>
        <LayoutContent>{children}</LayoutContent>
      </Layout>
    </Layout>
  );
}
