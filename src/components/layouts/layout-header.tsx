import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { Avatar, Button, Col, Layout, Row, Space, Switch, theme } from "antd";
import React, { useState } from "react";
const { useToken } = theme;
type Props = {
  setTheme :any;
};

export default function LayoutHeader({setTheme}: Props) {
  const { token } = useToken();
  
  return (
    <Layout.Header  style={{ backgroundColor: token.colorPrimaryBgHover }}>
      <Row>
        <Col
          span={24}
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <div>
            <MenuFoldOutlined />
          </div>

          <div>
            <Space>
              <span>Mac</span>
              <Avatar style={{ backgroundColor: token.colorPrimaryTextHover }}>M</Avatar>
            </Space>
            <Switch checkedChildren="LIGHT SIDE" onChange={(value:boolean)=>{
              setTheme(value)
            }} unCheckedChildren="DARK SIDE" defaultChecked />
          </div>
        </Col>
      </Row>
    </Layout.Header>
  );
}
