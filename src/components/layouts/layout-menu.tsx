import { PATH_ROUTE } from "@/src/constants/routes";
import {
  AppstoreOutlined,
  ContainerOutlined,
  DashboardOutlined,
  DesktopOutlined,
  MailOutlined,
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  PieChartOutlined,
} from "@ant-design/icons";
import { Menu, MenuProps } from "antd";
import { Layout } from "antd";
import { useRouter } from "next/router";
import { useState } from "react";

type Props = {};

export default function LayoutMenu({}: Props) {
  const [collapsed, setCollapsed] = useState(false);
  const route = useRouter();
  const toggleCollapsed = () => {
    setCollapsed(!collapsed);
  };
  type MenuItem = Required<MenuProps>["items"][number];

  function getItem(
    label: React.ReactNode,
    key: React.Key,
    icon?: React.ReactNode,
    children?: MenuItem[],
    type?: "group"
  ): MenuItem {
    return {
      key,
      icon,
      children,
      label,
      type,
    } as MenuItem;
  }

  const items: MenuItem[] = [
    getItem("DashBaord", PATH_ROUTE.DASHBOARD, <DashboardOutlined />),
    getItem("Products", PATH_ROUTE.PRODUCT, <DesktopOutlined />),
    getItem("Employee", PATH_ROUTE.EMPLOYEE, <ContainerOutlined />),

    getItem("Navigation One", "sub1", <MailOutlined />, [
      getItem("Option 5", "5"),
      getItem("Option 6", "6"),
      getItem("Option 7", "7"),
      getItem("Option 8", "8"),
    ]),

    getItem("Navigation Two", "sub2", <AppstoreOutlined />, [
      getItem("Option 9", "9"),
      getItem("Option 10", "10"),

      getItem("Submenu", "sub3", null, [
        getItem("Option 11", "11"),
        getItem("Option 12", "12"),
      ]),
    ]),
  ];
  return (
    <Layout.Sider theme="light">
      <img src="next.svg" alt="Italian Trulli"/>      <Menu
        selectedKeys={[route.asPath]}
        defaultOpenKeys={["sub1"]}
        mode="inline"
        inlineCollapsed={collapsed}
        items={items}
        onClick={(value)=> route.push(value.key)}
      />
    </Layout.Sider>
  );
}
