import { PlusOutlined, UploadOutlined } from "@ant-design/icons";
import create from "@ant-design/icons/lib/components/IconFont";
import {
  Button,
  Drawer,
  Space,
  Form,
  Row,
  Col,
  Input,
  Select,
  DatePicker,
  Switch,
  Upload,
  UploadProps,
  message,
  UploadFile,
} from "antd";
import { RcFile } from "antd/es/upload";
import axios, { HttpStatusCode } from "axios";
import { url } from "inspector";
import React, { useState } from "react";

type Props = {
  open: boolean;
  setOpen: any;
  type: string;
  setrefresh: React.Dispatch<React.SetStateAction<boolean>>
  refresh: boolean;
  dataEdit: any;
  form: any;
};

export default function DrawerAdd({ open, setOpen, type, setrefresh, refresh, dataEdit, form }: Props) {

  const [fileList, setFileList] = useState<UploadFile[]>([])


  React.useEffect(() => {


    console.log('type======:: ', type)
    console.log('dataEdit======:: ', dataEdit)
    setFileList(dataEdit?.files?.map((value: any, index: string) => {
      value.uid = index
      value.name = value.image_name
      value.url = value.image_url
      return value
    }))
  }, [dataEdit])



  const onClose = () => {
    setOpen(false);
    onReset();
  };
  const props: UploadProps = {
    name: 'files',
    headers: {
      authorization: 'authorization-text',
    }, onPreview() {
    },
    onChange(info) {
      if (info.file.status !== 'uploading') {
        console.log(info.file, info.fileList);
      }
      if (info.file.status === 'done') {
        setFileList(info.fileList)
        message.success(`${info.file.name} file uploaded successfully`);
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} file upload failed.`);
      }
      // let newFileList = [...info.fileList];

      // // 1. Limit the number of uploaded files
      // // Only to show two recent uploaded files, and old ones will be replaced by the new
      // newFileList = newFileList.slice(-2);

      // // 2. Read from response and show file link
      // newFileList = newFileList.map((file) => {
      //   if (file.response) {
      //     // Component will show file.url as link
      //     file.url = file.response.url;
      //   }
      //   return file;
      // });
      
      setFileList(info.fileList);
    },
  };
  const onReset = () => {
    form.resetFields();
  };

  const onSubmit = () => {
    form.submit()

  };

  const onFinish = (value: any) => {
    console.log(value)
    createFormData(value)
  }
  const createFormData = (valueData: any) => {
    const productFormData = new FormData();
    productFormData.append("p_name", valueData.p_name)
    productFormData.append("p_type", valueData.p_type)
    productFormData.append("p_color", valueData.p_color)
    productFormData.append("p_price", valueData.p_price)
    productFormData.append("p_size", valueData.p_size)
    productFormData.append("p_status", valueData.p_status)
    productFormData.append("p_description", valueData.p_description)
    fileList.forEach((element) => {
      console.log(element.originFileObj)
      if(element?.originFileObj){
        productFormData.append("files", element.originFileObj as RcFile)
      }
    })
    console.log("valueData:::", valueData)
    // console.log("files:::", dataEdit.files)
    if (dataEdit?.id) {
      // setFileList(dataEdit.files)
      productFormData.append("id", dataEdit.id)
      console.log("HAVEDATAEDT::", dataEdit.id)
      axios.patch(`http://localhost:4000/products/${dataEdit.id}`, productFormData, ).then(res => {
        console.log("FORM::", productFormData)
        if (res.status == HttpStatusCode.Ok) {
          setrefresh(!refresh)
        }
        console.log(res)
      }).catch((error => {
        console.log(error)
      }))
    } else {
      axios.post(`http://localhost:4000/products`, productFormData).then(response => {
        if (response.status == HttpStatusCode.Created) {
          setrefresh(!refresh)
        }
        console.log(response.data)
      }).catch((error => {
        console.log(error)
      }))
    }

  }

  return (
    <>
      <Drawer
        title={type === "UPDATE" ? "Update product" : "Create Product"}
        width={720}
        onClose={onClose}
        open={open}
        bodyStyle={{ paddingBottom: 80 }}
        footer={
          <div style={{ display: "flex", justifyContent: "end" }}>
            <Space>
              <Button onClick={() => setOpen(false)}>Cancel</Button>
              <Button onClick={() => onSubmit()} type="primary">
                Submit
              </Button>
            </Space>
          </div>
        }
      >
        <Form onFinish={
          onFinish
        } form={form} layout="vertical" >
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="p_name"
                label="Name"
                rules={[
                  { required: true, message: "Please enter product name" },
                ]}
              >
                <Input placeholder="Please enter product name" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="p_type"
                label="Type"
                rules={[
                  { required: true, message: "Please enter product type" },
                ]}
              >
                <Input placeholder="Please enter product type" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="p_color"
                label="Color"
                rules={[{ required: true, message: "Please enter color" }]}
              >
                <Input placeholder="Please enter color" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="p_price"
                label="Price"
                rules={[
                  { required: true, message: "Please enter product price" },
                ]}
              >
                <Input placeholder="Please enter product price" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="p_size"
                label="Size"
                rules={[{ required: true, message: "Please enter size" }]}
              >
                <Input placeholder="Please enter size" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="p_status"
                label="status"
              >
                <Select placeholder="Please select a bool">
                  <Select.Option value="true">True</Select.Option>
                  <Select.Option value="false">False</Select.Option>
                </Select>
              </Form.Item>




            </Col>
            <Col span={24}>
              <Form.Item
                name="p_description"
                label="Description"
                rules={[
                  { required: true, message: "Please enter description" },
                ]}
              >
                <Input.TextArea placeholder="Please enter description" />
              </Form.Item>
            </Col>
          </Row>
          <Upload {...props} fileList={fileList}>
            <Button icon={<UploadOutlined />}>Click to Upload</Button>
          </Upload>
        </Form>
      </Drawer>
    </>
  );
}
