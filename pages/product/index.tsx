import { Breadcrumb, Button, Card, Col, Row, Space, Table, Tag, Image, Form } from "antd";
import React, { useEffect, useState } from "react";
import type { ColumnsType } from "antd/es/table";
import { HomeOutlined, UserOutlined } from "@ant-design/icons";
import { PATH_ROUTE } from "@/src/constants/routes";
import Link from "next/link";
import DrawerAdd from "@/src/components/product/drawer_add";
import Eye from "@2fd/ant-design-icons/lib/Eye";
import CircleEditOutline from "@2fd/ant-design-icons/lib/CircleEditOutline";
import Delete from "@2fd/ant-design-icons/lib/Delete";
import axios, { HttpStatusCode } from 'axios';

type Props = {};

export default function Index({ }: Props) {
  const [form] = Form.useForm();
  const [open, setopen] = useState(false);
  const [dataProduct, setDataProduct] = useState<DataType[]>([])
  const [type, settype] = useState("")
  const [refresh, setrefresh] = useState(false)
  const [dataEdit, setdataEdit] = useState<any>()

  const onOpen = () => {
    setopen(true);
  };

  useEffect(() => {
    callApi()
  }, [refresh])


  const callApi = () => {
    axios.get(`http://localhost:4000/products`).then(response => {
      var dataTemp: DataType[] = []
      for (let i = 0; i < response.data.data.length; i++) {
        dataTemp.push({
          id: response.data.data[i].id,
          p_name: response.data.data[i].p_name,
          p_type: response.data.data[i].p_type,
          p_color: response.data.data[i].p_color,
          p_price: response.data.data[i].p_price,
          p_description: response.data.data[i].p_description,
          p_size: ["XL"],
          p_status: response.data.data[i].p_status,
          files: response.data.data[i].files
        });
      }

      setDataProduct(dataTemp)
      console.log(response.data)
      setdataEdit("")
      setopen(false)


    }).catch((error => {
      console.log(error)
    }))

  }

  const deleteApi = (id: string) => {
    console.log('id ==> ', id)
    axios.delete(`http://localhost:4000/products/${id}`).then(response => {
      console.log(response.data)
      console.log(response.status)
      if (response.status == HttpStatusCode.Ok) {
        callApi()
      }

    }).catch((error => {
      console.log(error)
    }))
  }





  interface DataType {
    id: string;
    p_name: string;
    p_type: string;
    p_color: string;
    p_price: string;
    p_description: string;
    p_size: string[];
    p_status: string;
    files: ImageType[]
  }

  interface ImageType {
    key: string;
    image_name: string;
    image_type: string;
    image_url: string;
  }


  const columns: ColumnsType<DataType> = [
    {
      title: "No.",
      render: (_, recorde, index) =>
        (<>{index + 1}</>)
    },
    {
      title: "Name",
      dataIndex: "p_name",
      key: "p_name",
      render: (text) => <a>{text}</a>,
    },
    {
      title: "Type",
      dataIndex: "p_type",
      key: "p_type",
    },
    {
      title: "Color",
      dataIndex: "p_color",
      key: "p_color",
    },
    {
      title: "Price",
      dataIndex: "p_price",
      key: "p_price",
    },
    {
      title: "Description",
      dataIndex: "p_description",
      key: "p_description",
    },
    {
      title: "Image",
      dataIndex: "files",
      key: "files",
      render: (_, { files }) => {
        return (_ && _.length && _.map((item: ImageType, index: number) => {
          return (
            <Image key={index} src={item.image_url} />
          )
        }))
      }

    },
    {
      title: "Size",
      dataIndex: "p_size",
      key: "p_size",
      render: (_, { p_size }) => (
        <>
          {p_size.map((size) => {
            let color = size.length > 5 ? 'geekblue' : 'green';
            if (size === 'loser') {
              color = 'volcano';
            }
            return (
              <Tag color={color} key={size}>
                {size.toUpperCase()}
              </Tag>
            );
          })}
        </>
      ),
    },
    {
      title: "Status",
      dataIndex: "p_status",
      key: "p_status",
    },
    {
      title: "Action",
      key: "action",
      render: (_, record) => (
        <Space size="middle">
          <Eye style={{ color: 'blue' }}
          />
          <CircleEditOutline style={{ color: 'orange' }} onClick={() => {
            console.log("record::",record)
            submitProduct('UPDATE', record)
          }}
          />
          <Delete style={{ color: 'red', cursor: 'pointer' }} onClick={() => deleteApi(_.id)}
          />
        </Space>
      ),
    },
  ];

  const submitProduct = (type: string, data?: any) => {
    settype(type)
    if (type === "UPDATE") {
      form.setFieldsValue({
        id: data?.id || "",
        p_name: data?.p_name || "",
        p_type: data?.p_type || "",
        p_color: data?.p_color || "",
        p_price: data?.p_price || "",
        p_size: data?.p_size || "",
        p_status: data?.p_status || "",
        p_description: data?.p_description || "",
        files: data?.p_description || "",
      })
    }
    setdataEdit(data)
    setopen(true)
  }
  return (
    <div style={{ margin: 10 }}>
      <Card>
        <Breadcrumb>
          <Breadcrumb.Item>
            <Link href={PATH_ROUTE.DASHBOARD}>
              <HomeOutlined />
            </Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item>Product</Breadcrumb.Item>
        </Breadcrumb>
        <br />
        <Row>
          <Col span={24} style={{ display: "flex", justifyContent: "end" }}>
            <Button
              type="primary"
              onClick={() => {
                submitProduct("CREATE")
              }}
            >
              Add Product
            </Button>
          </Col>
        </Row>
        <Table columns={columns} dataSource={dataProduct} />
      </Card>
      <DrawerAdd open={open} setOpen={setopen} type={type} setrefresh={setrefresh} refresh={refresh} dataEdit={dataEdit} form={form} />
    </div>
  );
}
