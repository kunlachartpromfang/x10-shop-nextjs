import "@/styles/globals.css";
import type { AppProps } from "next/app";
import "antd/dist/reset.css";
import LayoutMain from "@/src/components/layouts/layout-main";
import { ConfigProvider, Button, theme } from "antd";
import { useEffect, useState } from "react";

export default function App({ Component, pageProps }: AppProps) {
  const [theme, settheme] = useState(true);
  const light = {
    colorPrimary: "#50733a",
    colorPrimaryBg: "#aeb3a8",
    colorPrimaryBgHover: "#a1a69c",
    colorPrimaryBorder: "#949990",
    colorPrimaryBorderHover: "#7e8c73",
    colorPrimaryHover: "#668054",
    colorPrimaryActive: "#314d22",
    colorPrimaryTextHover: "#668054",
    colorPrimaryText: "#50733a",
    colorPrimaryTextActive: "#314d22",
    colorBgBase: "#ccdacf",
    colorText: "rgba(0, 0, 0, 0.88)",
    colorBgElevated: "#ccdacf",
    colorBgLayout: "#c0d1c4",
    colorBgMask: "rgba(0, 0, 0, 0.45)",
    colorBgSpotlight: "rgba(0, 0, 0, 0.85)",
  };

  const dark = {
    colorPrimary: "#50733a",
    colorPrimaryBg: "#aeb3a8",
    colorPrimaryBgHover: "#a1a69c",
    colorPrimaryBorder: "#949990",
    colorPrimaryBorderHover: "#7e8c73",
    colorPrimaryHover: "#668054",
    colorPrimaryActive: "#314d22",
    colorPrimaryTextHover: "#668054",
    colorPrimaryText: "#50733a",
    colorPrimaryTextActive: "#314d22",
    colorBgBase: "#9c9c9c",
    colorText: "rgba(0, 0, 0, 0.88)",
    colorBgElevated: "#ccdacf",
    colorBgLayout: "#c0d1c4",
    colorBgMask: "rgba(0, 0, 0, 0.45)",
    colorBgSpotlight: "rgba(0, 0, 0, 0.85)",
    colorBgContainer: "#b0b0b0",
    colorTextSecondary: "rgba(255, 255, 255, 0.65)",
    colorTextTertiary: "rgba(255, 255, 255, 0.45)",
    colorTextQuaternary: "rgba(255, 255, 255, 0.25)",
    colorBorder: "#dedede",
    colorBorderSecondary: "#cccccc",
    colorFill: "rgba(255, 255, 255, 0.18)",
    colorFillSecondary: "rgba(255, 255, 255, 0.12)",
    colorFillTertiary: "rgba(255, 255, 255, 0.08)",
    colorFillQuaternary: "rgba(255, 255, 255, 0.04)",
  };

  return (
    <>
      <ConfigProvider
        theme={{
          token: theme ? light : dark,
        }}
      >
        <LayoutMain setTheme={settheme}>
          <Component {...pageProps} />
        </LayoutMain>
      </ConfigProvider>
    </>
  );
}
